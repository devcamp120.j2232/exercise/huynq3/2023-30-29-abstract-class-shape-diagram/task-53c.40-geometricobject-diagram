import Model.Circle;
import Model.Rectangle;

public class GeometricObjectDiagram {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(2.0);
        Rectangle rectangle = new Rectangle(4, 5);
        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println("DT Circle: " + circle.getArea());
        System.out.println("Cv Circle: " + circle.getPerimeter());
        System.out.println("DT Rectangle: " + rectangle.getArea());
        System.out.println("CV Rectangle: " + rectangle.getPerimeter());
    }
}
